import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;


class DiceHand {

    private final int[] dice;

    public DiceHand(int d1, int d2, int d3, int d4, int d5) {
        this.dice = new int[]{d1, d2, d3, d4, d5};
    }

    private static Group map(Entry<Integer, Long> e) {
        return new Group(e.getKey(), e.getValue());
    }

    public int count(IntPredicate p) {
        return stream().filter(p).sum();
    }

    public IntStream stream() {
        return Arrays.stream(dice);
    }

    public Stream<Group> groups(Predicate<Group> p, Comparator<Group> sort, int quantity) {
        return groups()
            .filter(p)
            .sorted(sort)
            .limit(quantity);
    }

    private Stream<Group> groups() {
        final Map<Integer, Long> count = stream()
            .boxed()
            .collect(groupingBy(identity(), counting()));
        return count.entrySet()
            .stream()
            .map(DiceHand::map);
    }

}

class Group {

    private final int die;
    private final long count;

    public Group(int die, long count) {
        this.die = die;
        this.count = count;
    }

    public int getDie() {
        return die;
    }

    public long getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return die == group.die &&
            count == group.count;
    }

    @Override
    public int hashCode() {
        return Objects.hash(die, count);
    }

    public long sum() {
        return die * count;
    }

}

public class Yatzy {

    public static final List<Integer> SMALL_STRAIGHT = asList(1, 2, 3, 4, 5);
    public static final List<Integer> LARGE_STRAIGHT = asList(2, 3, 4, 5, 6);

    public static final Comparator<Group> BY_DIE = comparing(Group::getDie);
    public static final Comparator<Group> BY_DIE_REVERSED = BY_DIE.reversed();

    private final DiceHand diceHand;

    public Yatzy(DiceHand diceHand) {
        this.diceHand = diceHand;
    }

    private static boolean two(int n) {
        return n == 2;
    }

    private static boolean three(int n) {
        return n == 3;
    }

    private static boolean one(int n) {
        return n == 1;
    }

    private static boolean four(int n) {
        return n == 4;
    }

    private static boolean five(int n) {
        return n == 5;
    }

    private static boolean six(int n) {
        return n == 6;
    }

    public int fullHouse() {
        Optional<Group> three = diceHand.groups(g -> g.getCount() == 3, BY_DIE, 1)
            .findFirst();
        Optional<Group> two = diceHand.groups(g -> g.getCount() == 2, BY_DIE, 1)
            .findFirst();
        return three.flatMap(_3 -> two.map(_2 -> _3.sum() + _2.sum()))
            .map(Long::intValue)
            .orElse(0);
    }

    public int largeStraight() {
        return straight(LARGE_STRAIGHT);
    }

    public int smallStraight() {
        return straight(SMALL_STRAIGHT);
    }

    private int straight(List<Integer> straight) {
        List<Integer> list = diceHand.stream()
            .collect(ArrayList::new, List::add, List::addAll);
        return list.containsAll(straight) ? diceHand.stream().sum() : 0;
    }

    public int yatzy() {
        int first = diceHand.stream().iterator().nextInt();
        if (diceHand.stream().allMatch(n -> n == first)) {
            return 50;
        }
        return 0;
    }

    public int chance() {
        return diceHand.stream().sum();
    }

    public Integer four_of_a_kind() {
        return sumGroups(4, 1);
    }

    public Integer three_of_a_kind() {
        return sumGroups(3, 1);
    }

    public int two_pair() {
        return sumGroups(2, 2);
    }

    public int sumGroups(int count, int quantity) {
        List<Group> groups = diceHand.groups(e -> e.getCount() >= count, BY_DIE_REVERSED, quantity)
            .collect(toList());
        return groups.size() < quantity ? 0 : groups.stream()
            .map(Group::getDie)
            .reduce(0, (sum, die) -> sum + die * count, Integer::sum);
    }

    public int score_pair() {
        return sumGroups(2, 1);
    }

    public int ones() {
        return diceHand.count(Yatzy::one);
    }

    public int twos() {
        return diceHand.count(Yatzy::two);
    }

    public int threes() {
        return diceHand.count(Yatzy::three);
    }

    public int fours() {
        return diceHand.count(Yatzy::four);
    }

    public int fives() {
        return diceHand.count(Yatzy::five);
    }

    public int sixes() {
        return diceHand.count(Yatzy::six);
    }

}



